axios.defaults.baseURL = "http://big-event-vue-api-t.itheima.net"; 


axios.interceptors.request.use(function (config) {
    if (localStorage.getItem('token')) {
        config.headers.Authorization = localStorage.getItem('token')
    }
    return config
}, function (error) {
    return Promise.reject(error)
})


axios.interceptors.response.use(function (response) {
  
    return response.data
}, function (error) {
    return Promise.reject(error)
})