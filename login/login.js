document
  .querySelector('.layui-btn')
  .addEventListener('click', async function (e) {
    e.preventDefault()
    const data = serialize(document.querySelector('form'), { hash: true })
    const res = await axios({
      url: '/api/login',
      method: 'post',
      data
    })

    // 保存token
    localStorage.setItem('token', res.token)
    alert('登录成功，即将跳转首页')
    location.href = './index.html'
  })
