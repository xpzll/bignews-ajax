// 获取所有分类
async function getAllCate () {
  const res = await axios({
    url: '/my/cate/list'
  })

  // 新数据也必须保留所有分类，方便查询
  document.querySelector(
    '[name="cate_id"]'
  ).innerHTML = `<option value="">所有分类</option>`
  document.querySelector('[name="cate_id"]').innerHTML += res.data
    .map(v => `<option value="${v.id}">${v.cate_name}</option>`)
    .join('')
}
getAllCate()


document.querySelector('#btn-ok').addEventListener('click', async function (e) {
  e.preventDefault()
  const fm = new FormData(document.querySelector('form'))
  const res = await axios({
    url: '/my/article/add',
    method: 'post',
    data: fm
  })

  alert('新增成功！')
  document.querySelector('form').reset()
})