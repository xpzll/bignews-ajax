// 加载数据 -- 把参数当形参传进来
async function loadData (params) {
  const res = await axios({
    url: '/my/article/list',
    params
  })
  document.querySelector('tbody').innerHTML = res.data
    .map(
      v => `<tr>
        <td>
          <div class="cell">${v.title}</div>
        </td>
        <td>
          <div class="cell">${v.cate_name}</div>
        </td>
        <td>
          <div class="cell">${dayjs(v.pub_date).format('YYYY-MM-DD')}</div>
        </td>
        <td>
          <div class="cell">${v.state}</div>
        </td>
        <td>
          <div class="cell">
            <button
            data-id="${v.id}"
              class="layui-btn layui-btn-sm layui-bg-red btn-delete"
            >
              删除
            </button>
          </div>
        </td>
      </tr>`
    )
    .join('')
}

// 默认查出所有，不给cate_id和state就是所有
loadData({
  pagenum: 1,
  pagesize: 5
})

// 获取所有分类
async function getAllCate () {
  const res = await axios({
    url: '/my/cate/list'
  })

  // 新数据也必须保留所有分类，方便查询
  document.querySelector(
    '[name="cate_id"]'
  ).innerHTML = `<option value="">所有分类</option>`
  document.querySelector('[name="cate_id"]').innerHTML += res.data
    .map(v => `<option value="${v.id}">${v.cate_name}</option>`)
    .join('')
}
getAllCate()

// 筛选
document.querySelector('#btn-filter').addEventListener('click', function () {
  loadData({
    pagenum: 1,
    pagesize: 5,
    cate_id: document.querySelector('[name="cate_id"]').value,
    state: document.querySelector('[name="state"]').value
  })
})

// 修改
document.querySelector('tbody').addEventListener('click', async function (e) {
  if (e.target.classList.contains('btn-edit')) {
    location.href = './'
  }
})

// 删除
document.querySelector('tbody').addEventListener('click', async function (e) {
  if (e.target.classList.contains('btn-delete')) {
    if (confirm('确认删除吗？')) {
      await axios({
        url: '/my/article/info',
        method: 'delete',
        params: { id: e.target.dataset.id }
      })

      loadData({
        pagenum: 1,
        pagesize: 5
      })
    }
  }
})
