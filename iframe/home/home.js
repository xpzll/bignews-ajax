function initLine () {
  const myChart = echarts.init(document.querySelector('#curve_show'))

  const option = {
    title: {
      text: '月新增文章数',
      left: 'center'
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: [
        '2019-04-13',
        '2019-04-18',
        '2019-04-23',
        '2019-04-28',
        '2019-05-03',
        '2019-05-08'
      ]
    },
    yAxis: {
      type: 'value'
    },
    legend: {
      data: ['新增文章'],
      top: 30,
      left: 'center'
    },
    toolbox: {
      feature: {
        dataView: {},
        magicType: {
          type: ['line', 'bar']
        },
        restore: {},
        saveAsImage: {}
      }
    },
    series: [
      {
        name: '新增文章',
        data: [36, 52, 78, 85, 65, 72, 88, 64, 72, 90, 96, 100, 102, 110],
        type: 'line',
        itemStyle: {
          color: '#ffac4d'
        },
        areaStyle: {}
      }
    ]
  }

  myChart.setOption(option)
}
function initPie () {
  const myChart = echarts.init(document.querySelector('#pie_show'))

  const option = {
    title: {
      text: '分类文章数量比',
      left: 'center'
    },
    tooltip: {
      trigger: 'item'
    },
    legend: {
      top: 48,
      left: 'center'
    },
    toolbox: {
        left: 'center',
        top: 25,
        feature: {
          dataView: {},
          restore: {},
          saveAsImage: {}
        }
      },
    series: [
      {
        name: 'Access From',
        type: 'pie',
        radius: ['40%', '60%'],
        center: ['50%', '55%'],
        data: [
          { value: 1048, name: '奇趣事' },
          { value: 735, name: '会生活' },
          { value: 580, name: '爱旅行' },
          { value: 484, name: '趣美味' }
        ],
        emphasis: {
          itemStyle: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: 'rgba(0, 0, 0, 0.5)'
          }
        }
      }
    ]
  };

  myChart.setOption(option)
}


function initBar () {
  const myChart = echarts.init(document.querySelector('#column_show'))

  const option = {
    title: {
      text: '文章访问量',
      left: 'center'
    },
    color: ['#fea072', '#2ec4ec', '#13dfe6', '#00dd70'],
    tooltip: {
      trigger: 'axis'
    },
    grid: {
      boundaryGap: false
    },
    legend: {
      data: [ '奇趣事',  '会生活', '爱旅行','趣美味' ],
      top: 30
    },
    toolbox: {
        feature: {
          dataView: {},
          magicType: {
            type: ['line', 'bar']
          },
          restore: {},
          saveAsImage: {}
        }
      },
    xAxis: [
      {
        type: 'category',
        // prettier-ignore
        data: ['1月', '2月', '3月', '4月']
      }
    ],
    yAxis: [
      {
        type: 'value'
      }
    ],
    series: [
      {
        name: '奇趣事',
        type: 'bar',
        data: [
          800,
          650,
          700,
          900
        ]
      },
       {
        name: '会生活',
        type: 'bar',
        data: [
          400,
          350,
          550,
          442
        ]
      },
           {
        name: '爱旅行',
        type: 'bar',
        data: [
          440,
          560,
          330,
          277
        ]
      },
         {
        name: '趣美味',
        type: 'bar',
        data: [
          420,
          510,
          370,
          471
        ]
      },
    ]
  };

  myChart.setOption(option)
}


initLine()
initPie()
initBar()