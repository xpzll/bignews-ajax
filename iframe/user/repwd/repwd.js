document.querySelector('.btn-ok').addEventListener('click', async function (e) {
  e.preventDefault()

  const data = serialize(document.querySelector('form'), { hash: true })
  const res = await axios({
    url: '/my/updatepwd',
    method: 'patch',
    data
  })

  alert(res.message)
})
