async function loadData () {
  const res = await axios({
    url: '/my/userinfo'
  })
  for (const key in res.data) {
    console.log(key)
    if (key !== 'user_pic')
      document.querySelector(`[name=${key}]`).value = res.data[key]
  }
}

loadData()


document.querySelector('.btn-ok').addEventListener('click', async function (e) {
    e.preventDefault()
    const data = serialize(document.querySelector('form'), { hash: true })
    await axios({
        url:'/my/userinfo',
        method: 'put',
        data
    })
    alert('修改成功！')
})